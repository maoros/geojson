var granja = {
    "type": "FeatureCollection",
    "features": [
        {
            "geometry": {
                "type": "Point",
                "coordinates": [-74.13330, 4.57938]
            },
            "type": "Feature",
            "properties": {
                "popupContent": "Localización de la Granja de Pimentón"
            },
            "id": 50
        },
        ]
};

var comercial = {
    "type": "Feature",
    "properties": {
        "popupContent": "Este es el Centro Comercial Ciudad Tunal",
        "style": {
            weight: 2,
            color: "#777",
            opacity: 2,
            fillColor: "#0000FF",
            fillOpacity: 0.2
        }
    },
    "geometry": {
        "type": "MultiPolygon",
        "coordinates": [
            [
                [
                    [-74.13113218016045, 4.578269511813391],
                    [-74.13028778661344, 4.57868547680793],
                    [-74.12967290188001, 4.578334898800031],
                    [-74.12953055704664, 4.578059017324053],
                    [-74.1295552360779, 4.57775909723083],
                    [-74.12993651089283, 4.577265489136138],
                    [-74.13062118641786, 4.577002701366553],
                    [-74.1310018936113, 4.577929300783376]
                    ], ],[
                [
                   
                ]
            ]
        ]
    }
};
